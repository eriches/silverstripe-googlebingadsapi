<?php

namespace Hestec\GoogleBingAdsApi;

use SilverStripe\ORM\DataObject;

class RefreshToken extends DataObject {

    private static $table_name = 'GoogleBingAdsApiRefreshToken';

    private static $db = [
        'GoogleAds' => 'Text',
        'BingAds' => 'Text'
    ];

    function requireDefaultRecords(){
        parent::requireDefaultRecords();

        if (RefreshToken::get()->count() == 0){

            $record = new RefreshToken();
            $record->write();

        }

    }

}