<?php

namespace Hestec\GoogleBingAdsApi;

use SilverStripe\Security\Security;
use SilverStripe\Core\Config\Config;
use Illuminate\Console\Command;
use Google\Auth\OAuth2;
use Google\Auth\CredentialsLoader;

use Microsoft\BingAds\Auth\AuthorizationData;
use Microsoft\BingAds\Auth\OAuthTokenRequestException;
use Microsoft\BingAds\Auth\OAuthWebAuthCodeGrant;

use Microsoft\BingAds\V13\CustomerManagement\GetAccountsInfoRequest;
use Microsoft\BingAds\V13\CampaignManagement\ApplyOfflineConversionsRequest;
use Microsoft\BingAds\V13\CampaignManagement\OfflineConversion;

class GetRefreshToken extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'GoogleAuthorize',
        'BingAuthorize'
    );

    public function init() {
        parent::init();

        if (!Security::getCurrentUser()){
            return $this->httpError(404);
        }

    }

    public function GoogleAuthorize(){

        if (!$this->getRequest()->getSession()->get('oauth2') || empty($_GET['code'])) {

            $config = Config::inst()->get('GoogleBingAdsApi');

            // check if the config is right
            if (!$config) return 'Your Bing Ads config is not setup properly. Aborting.';

            $clientId = $config['ADWORDS_OAUTH2_CLIENT_ID'];
            $clientSecret = $config['ADWORDS_OAUTH2_CLIENT_SECRET'];
            $developerToken = $config['BING_DEVELOPER_TOKEN'];

            $scopes = 'https://www.googleapis.com/auth/adwords';
            $authorizationUri = 'https://accounts.google.com/o/oauth2/v2/auth';
            $redirectUri = 'https://'.$_SERVER['HTTP_HOST'].'/Hestec-GoogleBingAdsApi-GetRefreshToken/GoogleAuthorize';

            $oauth2 = new OAuth2([
                'authorizationUri' => $authorizationUri,
                'redirectUri' => $redirectUri,
                'tokenCredentialUri' => CredentialsLoader::TOKEN_CREDENTIAL_URI,
                'clientId' => $clientId,
                'clientSecret' => $clientSecret,
                'scope' => $scopes
            ]);

            $this->getRequest()->getSession()->set('oauth2', $oauth2);

            //$this->getRequest()->getSession()->set('state', $AuthorizationData->Authentication->State);

            $this->redirect($oauth2->buildFullAuthorizationUri());
            //echo $oauth2->buildFullAuthorizationUri();


        }else{

            $oauth2 = $this->getRequest()->getSession()->get('oauth2');

            $oauth2->setCode($_GET['code']);
            $authToken = $oauth2->fetchAuthToken();

            /*try
            {
                $AuthorizationData->Authentication->RequestOAuthTokensByResponseUri('https://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            }
            catch(OAuthTokenRequestException $e)
            {
                printf("Error: %s\n", $e->Error);
                printf("Description: %s\n", $e->Description);
            }*/

            if ($update = RefreshToken::get()->first()){

                if (isset($authToken['refresh_token'])) {

                    $update->GoogleAds = $authToken['refresh_token'];
                    $update->write();

                    return "RefreshToken generated or updated";

                }else{

                    return "No refresh_token in the authToken array. Possible the app/url has already access and you have to remove it in your Google account for a new refresh_token. See the WikiHestec.";

                }

            }else{

                return "No record found in the table 'GoogleBingAdsApiRefreshToken'";

            }

            return "There was an error";

        }

    }

    /**
     * googleAdsRefresh
     *
     */
    /*public function xgoogleAdsRefresh()
    {
        $config = config('google-ads')['OAUTH2'] ?? [];

        // check if the config is right
        if (!$config) return $this->error('Your Google Ads config is not setup properly. Aborting.');

        $clientId = $config['clientId'];
        $clientSecret = $config['clientSecret'];

        $scopes = 'https://www.googleapis.com/auth/adwords';
        $authorizationUri = 'https://accounts.google.com/o/oauth2/v2/auth';
        $redirectUri = 'urn:ietf:wg:oauth:2.0:oob';

        $oauth2 = new OAuth2([
            'authorizationUri' => $authorizationUri,
            'redirectUri' => $redirectUri,
            'tokenCredentialUri' => CredentialsLoader::TOKEN_CREDENTIAL_URI,
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'scope' => $scopes
        ]);

        // print first message
        $this->line(sprintf(
            "Please sign in to your Google Ads account, and open following url:\n%s",
            $oauth2->buildFullAuthorizationUri([
                'access_type' => 'offline'
            ])
        ));

        // Retrieve token
        $accessToken = $this->ask('Insert your access token');

        // Fetch auth token
        try
        {
            $oauth2->setCode($accessToken);
            $authToken = $oauth2->fetchAuthToken();
        }
        catch (Exception $exception) {
            return $this->error($exception->getMessage());
        }

        if (!isset($authToken)) {
            return $this->error('Error fetching the refresh token');
        }

        $this->comment('Copy the refresh token and paste the value on ADWORDS_OAUTH2_REFRESH_TOKEN in your .env');

        // Print refresh token
        $this->line(sprintf(
            'Refresh token: "%s"',
            $authToken['refresh_token']
        ));
    }*/

    public function BingAuthorize(){

        if (!$this->getRequest()->getSession()->get('AuthorizationData') || !$this->getRequest()->getSession()->get('state') || empty($_GET['state'])) {

        $config = Config::inst()->get('GoogleBingAdsApi');

        // check if the config is right
        if (!$config) return 'Your Bing Ads config is not setup properly. Aborting.';

        $clientId = $config['BING_CLIENT_ID'];
        $clientSecret = $config['BING_CLIENT_SECRET'];
        $developerToken = $config['BING_DEVELOPER_TOKEN'];

        $authentication = (new OAuthWebAuthCodeGrant())
            ->withClientId($clientId)
            ->withClientSecret($clientSecret)
            ->withRedirectUri('https://'.$_SERVER['HTTP_HOST'].'/Hestec-GoogleBingAdsApi-GetRefreshToken/BingAuthorize')
            ->withState(rand(0, 999999999));

        $AuthorizationData = (new AuthorizationData())
            ->withAuthentication($authentication)
            ->withDeveloperToken($developerToken);

        $this->getRequest()->getSession()->set('AuthorizationData', $AuthorizationData);

        $this->getRequest()->getSession()->set('state', $AuthorizationData->Authentication->State);

        $this->redirect($AuthorizationData->Authentication->GetAuthorizationEndpoint());

        }else{

            if ($_GET['state'] != $_SESSION['state']){

                return "The OAuth response state (".$_GET['state'].") does not match the client request state (".$_SESSION['state'].")";

            }

            $AuthorizationData = $this->getRequest()->getSession()->get('AuthorizationData');

            try
            {
                $AuthorizationData->Authentication->RequestOAuthTokensByResponseUri('https://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            }
            catch(OAuthTokenRequestException $e)
            {
                printf("Error: %s\n", $e->Error);
                printf("Description: %s\n", $e->Description);
            }

            if ($update = RefreshToken::get()->first()){

                $update->BingAds = $AuthorizationData->Authentication->OAuthTokens->RefreshToken;
                $update->write();

                return "RefreshToken generated or updated";


            }else{

                return "No record found in the table 'GoogleBingAdsApiRefreshToken'";

            }

            return "There was an error";

        }

    }

}
