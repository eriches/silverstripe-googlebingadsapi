<?php

namespace Hestec\GoogleBingAdsApi;

use Google\Ads\GoogleAds\Lib\OAuth2TokenBuilder;
use SilverStripe\Core\Config\Config;
use Google\Ads\GoogleAds\Lib\V18\GoogleAdsClientBuilder;
use Google\Ads\GoogleAds\V18\Services\ClickConversion;
//use Google\AdsApi\Common\AdsSession;
use Google\Ads\GoogleAds\Lib\V18\GoogleAdsException;
use Google\ApiCore\ApiException;
use Google\Ads\GoogleAds\Util\V18\ResourceNames;
use Psr\Log\LoggerInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Director;
use SilverStripe\Control\Email\Email;
use Google\Ads\GoogleAds\V18\Services\UploadClickConversionsRequest;

class GoogleConversion {

    private $ConversionTime;
    private $ConversionValue;
    private $ConversionClickId;

    public function add(){

        $config = Config::inst()->get('GoogleBingAdsApi');
        $customer_id_stripped = str_replace('-','',$config['ADWORDS_CLIENT_CUSTOMER_ID']);
        //$customer_id_stripped = $config['ADWORDS_CLIENT_CUSTOMER_ID'];

        $oAuth2Credential = (new OAuth2TokenBuilder())->withClientId($config['ADWORDS_OAUTH2_CLIENT_ID'])->withClientSecret($config['ADWORDS_OAUTH2_CLIENT_SECRET'])->withRefreshToken(RefreshToken::get()->first()->GoogleAds)->build();
        $googleAdsClient = (new GoogleAdsClientBuilder())->withDeveloperToken($config['ADWORDS_DEVELOPER_TOKEN'])->withOAuth2Credential($oAuth2Credential)->build();
        $conversionUploadServiceClient = $googleAdsClient->getConversionUploadServiceClient();

        $add = new ClickConversion();
        $add->setConversionDateTime($this->ConversionTime);
        $add->setConversionValue($this->ConversionValue);
        $add->setGclid($this->ConversionClickId);
        $add->setCurrencyCode('EUR');
        $add->setConversionAction(ResourceNames::forConversionAction($customer_id_stripped, $config['ADWORDS_CONVERSION_ACTION_ID']));

        $request = new UploadClickConversionsRequest([
            'customer_id' => $customer_id_stripped,
            'conversions' => [$add],
            'partial_failure' => true, // Stel deze in naar behoefte
        ]);

        try {
            $response = $conversionUploadServiceClient->uploadClickConversions($request);
        }catch (GoogleAdsException $googleAdsException) {

            $err = '';
            foreach ($googleAdsException->getGoogleAdsFailure()->getErrors() as $error) {
                $err .= $error->getErrorCode()->getErrorCode().": ";
                $err .= $error->getMessage()." | ";
            }

            $email_subject = "GoogleAds conversion error [1] in: GoogleConversion->add";
            $email_body = "ConversionClickId: ".$this->ConversionClickId." | RequestID: ".$googleAdsException->getRequestId()." | Error(s): ".$err;

            if (Director::isDev()) {
                Injector::inst()->get(LoggerInterface::class)->error($email_subject." >> ".$email_body);
            } else {
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), $email_subject, $email_body);
                $email->sendPlain();
            }
            return false;


        }catch (ApiException $apiException) {

            $email_subject = "GoogleAds conversion error [2] in: GoogleConversion->add";
            $email_body = $apiException->getMessage()." | ConversionClickId: ".$this->ConversionClickId;

            if (Director::isDev()) {
                Injector::inst()->get(LoggerInterface::class)->error($email_subject." >> ".$email_body);
            } else {
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), $email_subject, $email_body);
                $email->sendPlain();
            }
            return false;

        }

        if ($response->hasPartialFailureError()) {

            $email_subject = "GoogleAds conversion error [3] in: GoogleConversion->add";
            $email_body = $response->getPartialFailureError()->getMessage()." | ConversionClickId: ".$this->ConversionClickId;

            if (Director::isDev()) {
                Injector::inst()->get(LoggerInterface::class)->error($email_subject." >> ".$email_body);
            } else {
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), $email_subject, $email_body);
                $email->sendPlain();
            }
            return false;

        }
        return true;

    }

    public function setConversionTime($ConversionTime){
        $saletime = new \DateTime($ConversionTime);
        $this->ConversionTime = $saletime->format('Y-m-d H:i:sP'); // P = timezone offset
    }

    public function setConversionValue($ConversionValue){
        $this->ConversionValue = $ConversionValue;
    }

    public function setConversionClickId($ConversionClickId){
        $this->ConversionClickId = $ConversionClickId;
    }

}
