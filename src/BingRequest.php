<?php

namespace Hestec\GoogleBingAdsApi;

use SilverStripe\Core\Config\Config;
use Microsoft\BingAds\Auth\OAuthDesktopMobileAuthCodeGrant;
use Microsoft\BingAds\Auth\AuthorizationData;
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\OAuthTokenRequestException;
use Psr\Log\LoggerInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Director;
use SilverStripe\Control\Email\Email;

class BingRequest {


    public function Connect($serviceclienttype)
    {

        if (strlen(RefreshToken::get()->first()->BingAds) > 10 && $config = Config::inst()->get('GoogleBingAdsApi')) {

            $developerToken = $config['BING_DEVELOPER_TOKEN'];
            $clientId = $config['BING_CLIENT_ID'];
            $clientSecret = $config['BING_CLIENT_SECRET'];
            $refreshToken = RefreshToken::get()->first()->BingAds;
            $accountId = $config['BING_ACCOUNT_ID'];
            $CustomerId = $config['BING_CUSTOMER_ID'];

            try {

                $authentication = (new OAuthDesktopMobileAuthCodeGrant())
                    ->withClientId($clientId)
                    ->withClientSecret($clientSecret);

                $authorizationData = (new AuthorizationData())
                    ->withAuthentication($authentication)
                    ->withDeveloperToken($developerToken)
                    ->withCustomerId($CustomerId)
                    ->withAccountId($accountId);

                $authorizationData->Authentication->RequestOAuthTokensByRefreshToken($refreshToken);
                $customerManagementProxy = new ServiceClient(
                    $serviceclienttype,
                    $authorizationData,
                    'Production'
                );

                return $customerManagementProxy;


            } catch (OAuthTokenRequestException $e) {

                $email_subject = "BingAds conversion error in: BingRequest->connect";
                $email_body = $e->Error." | ".$e->Description;

                if (Director::isDev()) {
                    Injector::inst()->get(LoggerInterface::class)->error($email_subject." >> ".$email_body);
                } else {
                    $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), $email_subject, $email_body);
                    $email->sendPlain();
                }
                return false;

            }
        }
        return false;
    }

}
