<?php

namespace Hestec\GoogleBingAdsApi;

use Microsoft\BingAds\Auth\ServiceClientType;
use Microsoft\BingAds\V13\CampaignManagement\ApplyOfflineConversionsRequest;
use Microsoft\BingAds\V13\CampaignManagement\OfflineConversion;
use SilverStripe\Core\Config\Config;
use Psr\Log\LoggerInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Director;
use SilverStripe\Control\Email\Email;

class BingConversion {

    private $ConversionTime;
    private $ConversionValue;
    private $ConversionClickId;

    public function add(){

        $config = Config::inst()->get('GoogleBingAdsApi');

        $offlineConversion = array();
        $add = new OfflineConversion();
        $add->ConversionCurrencyCode = 'EUR';
        $add->ConversionName = $config['BING_CONVERSION_ACTION_NAME'];
        $add->ConversionTime = $this->ConversionTime;
        $add->ConversionValue = $this->ConversionValue;
        $add->MicrosoftClickId = $this->ConversionClickId;
        $offlineConversion[] = $add;

        $request = new ApplyOfflineConversionsRequest();

        $request->OfflineConversions = $offlineConversion;

        $serviceclienttype = ServiceClientType::CampaignManagementVersion13;

        $c = new BingRequest();
        if ($c->Connect($serviceclienttype) != false) {

            $output = $c->Connect($serviceclienttype)->GetService()->ApplyOfflineConversions($request);
            $status = json_decode(json_encode($output), true);

            if (!empty($status['PartialErrors'])) {

                $email_subject = "BingAds conversion error in: BingConversion->add";
                $email_body = $status['PartialErrors']['BatchError'][0]['Message']." | ConversionClickId: ".$this->ConversionClickId;

                if (Director::isDev()) {
                    Injector::inst()->get(LoggerInterface::class)->error($email_subject." >> ".$email_body);
                } else {
                    $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), $email_subject, $email_body);
                    $email->sendPlain();
                }
                return false;

            }
            return true;

        }
        return false;

    }

    public function setConversionTime($ConversionTime){
        $saletime = new \DateTime($ConversionTime);
        $this->ConversionTime = $saletime->getTimestamp();;
    }

    public function setConversionValue($ConversionValue){
        $this->ConversionValue = $ConversionValue;
    }

    public function setConversionClickId($ConversionClickId){
        $this->ConversionClickId = $ConversionClickId;
    }

}
