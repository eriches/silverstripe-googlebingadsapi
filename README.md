# SilverStripe GoogleBingAdsApi #

Add conversions to Google Ads and Microsoft Ads by their API.

### Requirements ###

* SilverStripe 4<br>
* PHP 7

### Version ###

Using Semantic Versioning.

### Installation ###

Install via Composer:

composer require "hestec/silverstripe-googlebingadsapi": "1.*"

### Configuration ###

Get refresh tokens:

* https://www.domain.xyz/Hestec-GoogleBingAdsApi-GetRefreshToken/BingAuthorize
* https://www.domain.xyz/Hestec-GoogleBingAdsApi-GetRefreshToken/GoogleAuthorize

### Working ###


### Usage ###


### Issues ###

No known issues.

### Todo ###


